/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mouse.assistant;

import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author Peyman Barjoueian
 */
public class MouseAssistant {

    /**
     * @param args the command line arguments
     * @throws MalformedURLException
     */
    public static void main(String[] args) throws MalformedURLException {
        try {       
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MouseAssistant.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(MouseAssistant.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(MouseAssistant.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(MouseAssistant.class.getName()).log(Level.SEVERE, null, ex);
        }
 

        MainFrame mainFrame = new MainFrame();
        mainFrame.setVisible(true);



    }

}
