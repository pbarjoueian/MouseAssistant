/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mouse.assistant;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Peyman Barjoueian
 */
public class ClickerMachine implements Runnable {

    private static boolean stopper;
    private Robot myRobot;
    private long clickNumber;
    private int clickTime;
    private int timeList;
    private int clickInterval;
    private int mouseButton;
    private int mouseAction;
    private int state;
    int counter = 0;

    public ClickerMachine(long clickNumber, int clickInterval, int mouseButton, int mouseAction, int state) {
        System.gc();
        this.clickNumber = clickNumber;
        this.clickInterval = clickInterval;
        this.mouseButton = mouseButton;
        this.mouseAction = mouseAction;
        this.state = state;
    }

    public ClickerMachine(int clickTime, int timeList, int clickInterval, int mouseButton, int mouseAction, int state) {
        System.gc();
        this.clickTime = clickTime;
        this.timeList = timeList;
        this.clickInterval = clickInterval;
        this.mouseButton = mouseButton;
        this.state = state;
    }

    @Override
    public void run() {
        try {
            myRobot = new Robot();
        } catch (AWTException ex) {
            Logger.getLogger(ClickerMachine.class.getName()).log(Level.SEVERE, null, ex);   
        }

        switch (state) {
            case 1:
                try {
                    robotJob(clickNumber, clickInterval, mouseButton, mouseAction);
                } catch (AWTException ex) {
                    Logger.getLogger(ClickerMachine.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case 2:
                robotJob(clickTime, timeList, clickInterval, mouseButton, mouseAction);
                break;
        }

    }

    public static void startUp() {
        stopper = true;
    }

    public static void shutDownThread() {

        stopper = false;

    }

    private void robotJob(long number, int interval, int button, int action) throws AWTException {

        while (stopper && counter < number) {
            switch (button) {
                case 0: {
                    switch (action) {
                        case 0: {
                            myRobot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
                            myRobot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
                            counter++;
                            MainFrame.showCounter(counter);
                            myRobot.delay(interval);
                            break;
                        }
                        case 1: {
                            myRobot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
                            myRobot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
                            myRobot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
                            myRobot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
                            counter++;
                            MainFrame.showCounter(counter);
                            myRobot.delay(interval);
                            break;
                        }
                        case 2: {
                            myRobot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
                            counter++;
                            MainFrame.showCounter(counter);
                            myRobot.delay(interval);
                            break;
                        }
                        case 3: {
                            myRobot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
                            counter++;
                            MainFrame.showCounter(counter);
                            myRobot.delay(interval);
                            break;
                        }
                    }
                    break;
                }
                case 1: {
                    switch (action) {
                        case 0: {
                            myRobot.mousePress(InputEvent.BUTTON2_DOWN_MASK);
                            myRobot.mouseRelease(InputEvent.BUTTON2_DOWN_MASK);
                            counter++;
                            MainFrame.showCounter(counter);
                            myRobot.delay(interval);
                            break;
                        }
                        case 1: {
                            myRobot.mousePress(InputEvent.BUTTON2_DOWN_MASK);
                            myRobot.mouseRelease(InputEvent.BUTTON2_DOWN_MASK);
                            myRobot.mousePress(InputEvent.BUTTON2_DOWN_MASK);
                            myRobot.mouseRelease(InputEvent.BUTTON2_DOWN_MASK);
                            counter++;
                            MainFrame.showCounter(counter);
                            myRobot.delay(interval);
                            break;
                        }
                        case 2: {
                            myRobot.mousePress(InputEvent.BUTTON2_DOWN_MASK);
                            counter++;
                            MainFrame.showCounter(counter);
                            myRobot.delay(interval);
                            break;
                        }
                        case 3: {
                            myRobot.mouseRelease(InputEvent.BUTTON2_DOWN_MASK);
                            counter++;
                            MainFrame.showCounter(counter);
                            myRobot.delay(interval);
                            break;
                        }
                    }
                    break;
                }
                case 2: {
                    switch (action) {
                        case 0: {
                            myRobot.mousePress(InputEvent.BUTTON3_DOWN_MASK);
                            myRobot.mouseRelease(InputEvent.BUTTON3_DOWN_MASK);
                            counter++;
                            MainFrame.showCounter(counter);
                            myRobot.delay(interval);
                            break;
                        }
                        case 1: {
                            myRobot.mousePress(InputEvent.BUTTON3_DOWN_MASK);
                            myRobot.mouseRelease(InputEvent.BUTTON3_DOWN_MASK);
                            myRobot.mousePress(InputEvent.BUTTON3_DOWN_MASK);
                            myRobot.mouseRelease(InputEvent.BUTTON3_DOWN_MASK);
                            counter++;
                            MainFrame.showCounter(counter);
                            myRobot.delay(interval);
                            break;
                        }
                        case 2: {
                            myRobot.mousePress(InputEvent.BUTTON3_DOWN_MASK);
                            counter++;
                            MainFrame.showCounter(counter);
                            myRobot.delay(interval);
                            break;
                        }
                        case 3: {
                            myRobot.mouseRelease(InputEvent.BUTTON3_DOWN_MASK);
                            counter++;
                            MainFrame.showCounter(counter);
                            myRobot.delay(interval);
                            break;
                        }
                    }
                    break;
                }
            }
        }
        if (counter >= number) {
            MainFrame.stopLayout();
        }

    }

    private void robotJob(int time, int tList, int interval, int button, int action) {
        Timer timer;
        class TimeOut extends TimerTask {

            @Override
            public void run() {
                MainFrame.stopLayout();
                shutDownThread();
            }
        }

        switch (tList) {
            case 1: {
                time = time * 1000;
                break;
            }
            case 2: {
                time = time * 60 * 1000;
                break;
            }
            case 3: {
                time = time * 60 * 60 * 1000;
                break;
            }

        }
        timer = new Timer();
        timer.schedule(new TimeOut(), time);

        while (stopper) {
            switch (button) {
                case 0: {
                    switch (action) {
                        case 0: {
                            myRobot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
                            myRobot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
                            counter++;
                            MainFrame.showCounter(counter);
                            myRobot.delay(interval);
                            break;
                        }
                        case 1: {
                            myRobot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
                            myRobot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
                            myRobot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
                            myRobot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
                            counter++;
                            MainFrame.showCounter(counter);
                            myRobot.delay(interval);
                            break;
                        }
                        case 2: {
                            myRobot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
                            counter++;
                            MainFrame.showCounter(counter);
                            myRobot.delay(interval);
                            break;
                        }
                        case 3: {
                            myRobot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
                            counter++;
                            MainFrame.showCounter(counter);
                            myRobot.delay(interval);
                            break;
                        }
                    }
                    break;
                }
                case 1: {
                    switch (action) {
                        case 0: {
                            myRobot.mousePress(InputEvent.BUTTON2_DOWN_MASK);
                            myRobot.mouseRelease(InputEvent.BUTTON2_DOWN_MASK);
                            counter++;
                            MainFrame.showCounter(counter);
                            myRobot.delay(interval);
                            break;
                        }
                        case 1: {
                            myRobot.mousePress(InputEvent.BUTTON2_DOWN_MASK);
                            myRobot.mouseRelease(InputEvent.BUTTON2_DOWN_MASK);
                            myRobot.mousePress(InputEvent.BUTTON2_DOWN_MASK);
                            myRobot.mouseRelease(InputEvent.BUTTON2_DOWN_MASK);
                            counter++;
                            MainFrame.showCounter(counter);
                            myRobot.delay(interval);
                            break;
                        }
                        case 2: {
                            myRobot.mousePress(InputEvent.BUTTON2_DOWN_MASK);
                            counter++;
                            MainFrame.showCounter(counter);
                            myRobot.delay(interval);
                            break;
                        }
                        case 3: {
                            myRobot.mouseRelease(InputEvent.BUTTON2_DOWN_MASK);
                            counter++;
                            MainFrame.showCounter(counter);
                            myRobot.delay(interval);
                            break;
                        }
                    }
                    break;
                }
                case 2: {
                    switch (action) {
                        case 0: {
                            myRobot.mousePress(InputEvent.BUTTON3_DOWN_MASK);
                            myRobot.mouseRelease(InputEvent.BUTTON3_DOWN_MASK);
                            counter++;
                            MainFrame.showCounter(counter);
                            myRobot.delay(interval);
                            break;
                        }
                        case 1: {
                            myRobot.mousePress(InputEvent.BUTTON3_DOWN_MASK);
                            myRobot.mouseRelease(InputEvent.BUTTON3_DOWN_MASK);
                            myRobot.mousePress(InputEvent.BUTTON3_DOWN_MASK);
                            myRobot.mouseRelease(InputEvent.BUTTON3_DOWN_MASK);
                            counter++;
                            MainFrame.showCounter(counter);
                            myRobot.delay(interval);
                            break;
                        }
                        case 2: {
                            myRobot.mousePress(InputEvent.BUTTON3_DOWN_MASK);
                            counter++;
                            MainFrame.showCounter(counter);
                            myRobot.delay(interval);
                            break;
                        }
                        case 3: {
                            myRobot.mouseRelease(InputEvent.BUTTON3_DOWN_MASK);
                            counter++;
                            MainFrame.showCounter(counter);
                            myRobot.delay(interval);
                            break;
                        }
                    }
                    break;
                }
            }
        }
        MainFrame.stopLayout();
    }
}
